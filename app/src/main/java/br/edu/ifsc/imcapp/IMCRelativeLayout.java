package br.edu.ifsc.imcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class IMCRelativeLayout extends AppCompatActivity {

    View v;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imcrelative_layout);
        final float[] imc = new float[1];
        Button btnCalcular=(Button) findViewById(R.id.btnCalc);
        Button btnCreditos= (Button) findViewById(R.id.btnCreditos);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView txtPeso = (TextView) findViewById(R.id.viewPeso);
                TextView txtAltura = (TextView) findViewById(R.id.viewAlt);
                TextView resultado = (TextView) findViewById(R.id.viewIMC);

                float peso = Float.parseFloat(txtPeso.getText().toString());
                float altura = Float.parseFloat(txtAltura.getText().toString());
                imc[0] = (float) (peso / Math.pow(altura, 2));
                if (imc[0] < 18.5) {
                    resultado.setText(imc[0] + " - Abaixo do peso");
                } else if (imc[0] < 25) {
                    resultado.setText(imc[0] + " - Peso ideal");
                } else if (imc[0] < 30) {
                    resultado.setText(imc[0] + " - Sobrepeso");
                } else if (imc[0] < 35) {
                    resultado.setText(imc[0] + " - Obesidade 1");
                } else if (imc[0] < 40) {
                    resultado.setText(imc[0] + " - Obesidade 2");
                } else {
                    resultado.setText(imc[0] + " - Obesidade 3");
                }
            }
        });
        btnCreditos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proximaTela(v);
            }
        });
    }


    public void proximaTela(View v){
        Intent i=new Intent(this,MainActivity.class);
        startActivity(i);
    }


}

